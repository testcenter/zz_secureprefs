package at.testcenter.android.securePrefs;

import java.util.HashMap;
import java.util.Map;

import android.content.SharedPreferences;
import at.testcenter.android.securePrefs.crypter.Crypter;

public final class SecureSharedPrefs {

	private SharedPreferences prefs;
	private Crypter crypterValues;
	private Crypter crypterKeys;

	/**
	 * see {@link #SecureSharedPrefs(SharedPreferences, Crypter, Crypter)}
	 * 
	 * @param prefs
	 * @param crypterKeys
	 * @param crypterValue
	 * @param overwrite
	 *            if false, it is the same as the other constuctor <br>
	 *            true to overwrite the given prefs file with the new crypter
	 */
	public SecureSharedPrefs(SharedPreferences prefs, Crypter crypterKeys, Crypter crypterValue, boolean overwrite) {
		this(prefs, crypterKeys, crypterValue);

		if (!overwrite) {
			return;
		}

		doImport();
	}

	private void doImport() {
		Map<String, ?> toImport = prefs.getAll();

		// clear the old prefs
		prefs.edit().clear().commit();

		for (String s : toImport.keySet()) {
			Object o = toImport.get(s);
			putValueInterally(s, o);
		}
	}

	/**
	 * default constructor
	 * 
	 * @param prefs
	 *            preferences which should be secure
	 * @param crypterKeys
	 *            crypter to obfuscate keys (null to do nothing) NOTE: no decrypt method necessary!
	 * @param crypterValues
	 *            crypter to obfuscate values (null to do nothing)
	 */
	public SecureSharedPrefs(SharedPreferences prefs, Crypter crypterKeys, Crypter crypterValues) {
		if (prefs == null || (crypterValues == null && crypterKeys == null)) {
			throw new IllegalArgumentException("wrong constructor parameters!!");
		}
		this.prefs = prefs;
		this.crypterValues = crypterValues;
		this.crypterKeys = crypterKeys;
	}

	/**
	 * NOTE: you can not put null values!
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public <T extends Number> boolean putNumber(String key, T value) {
		return putValueInterally(key, value);
	}

	public boolean putBoolean(String key, boolean value) {
		return putValueInterally(key, value);
	}

	/**
	 * NOTE: you can not put null values!
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean putString(String key, String value) {
		return putValueInterally(key, value);
	}

	private boolean putValueInterally(String key, Object value) {
		if (key == null || key.length() <= 0 || value == null) {
			return false;
		}
		try {
			String cryptedValue = encryptValue(value.toString());
			String cryptedKey = encryptKey(key);
			return prefs.edit().putString(cryptedKey, cryptedValue).commit();
		} catch (Exception ex) {
			// exception while encrypting -> return false
		}
		return false;
	}

	/**
	 * NOTE: you can not put null values!
	 * 
	 * @param key
	 * @param value
	 * @param mapper
	 * @return
	 */
	public <T> boolean putObject(String key, T value, Mapper<T> mapper) {
		if (key == null || key.length() <= 0 || value == null || mapper == null) {
			return false;
		}
		String mappedString = mapper.marshall(value);
		return putValueInterally(key, mappedString);
	}

	@SuppressWarnings("unchecked")
	public <T extends Number> T getNumber(String key, T defaultValue) {
		String value = getObjectInternally(key);
		if (value == null) {
			return defaultValue;
		}

		Number retVal = null;
		if (defaultValue instanceof Integer) {
			retVal = Integer.parseInt(value.toString());
		} else if (defaultValue instanceof Float) {
			retVal = Float.parseFloat(value.toString());
		} else if (defaultValue instanceof Long) {
			retVal = Long.parseLong(value.toString());
		} else if (defaultValue instanceof Double) {
			retVal = Double.parseDouble(value.toString());
		} else {
			return defaultValue;
		}
		return (T) retVal;
	}

	public String getString(String key, String defaultValue) {
		String value = getObjectInternally(key);
		if (value == null) {
			return defaultValue;
		}
		return value.toString();
	}

	public Boolean getBoolean(String key, boolean defaultValue) {
		String value = getObjectInternally(key);
		if (value == null) {
			return defaultValue;
		}
		Boolean retVal = Boolean.parseBoolean(value.toString());
		return retVal;
	}

	public <T> T getObject(String key, Mapper<T> m, T defaultValue) {
		String value = getObjectInternally(key);
		if (value == null) {
			return defaultValue;
		}
		T retVal = m.unmarshall(value);
		return retVal;
	}

	private String getObjectInternally(String key) {
		if (key == null || key.length() <= 0) {
			return null;
		}
		try {
			String encryptedKey = encryptKey(key);
			String value = prefs.getString(encryptedKey, null);
			// pref not found
			if (value == null) {
				return null;
			}

			String retVal = decryptValue(value);
			return retVal;
		} catch (Exception ex) {
			// exception while decrypting
		}
		return null;
	}

	public Map<String, Object> getAll() {
		// get all stored prefs
		Map<String, ?> allObfuscated = prefs.getAll();
		// create map to return
		Map<String, Object> all = new HashMap<String, Object>(allObfuscated.size());

		// go through all stored prefs
		for (String key : allObfuscated.keySet()) {
			// do get the original key
			String originalKey;
			try {
				originalKey = decryptKey(key);
			} catch (Exception e) {
				originalKey = key;
			}
			// get the given value from the key (will be obfuscated again and checked)
			Object value = getObjectInternally(originalKey);
			// put the originalKey + the originalValue
			all.put(originalKey, value);
		}
		return all;
	}

	public boolean contains(String key) {
		return getObjectInternally(key) != null;
	}

	public boolean remove(String key) {
		String keyToRemove;
		try {
			keyToRemove = encryptKey(key);
		} catch (Exception ex) {
			keyToRemove = key;
		}
		return prefs.edit().remove(keyToRemove).commit();
	}

	private String encryptValue(String value) throws Exception {
		// do not encrypt / decrpyt if the crypter is null!
		if (crypterValues == null) {
			return value;
		}
		return crypterValues.encrypt(value);
	}

	private String decryptValue(String value) throws Exception {
		// do not encrypt / decrpyt if the crypter is null!
		if (crypterValues == null) {
			return value;
		}
		return crypterValues.decrypt(value);
	}

	private String encryptKey(String key) throws Exception {
		// do not encrypt / decrpyt if the crypter is null!
		if (crypterKeys == null) {
			return key;
		}
		return crypterKeys.encrypt(key);
	}

	private String decryptKey(String key) throws Exception {
		// do not encrypt / decrypt if the crypter is null!
		if (crypterKeys == null) {
			return key;
		}
		return crypterKeys.decrypt(key);
	}
}
