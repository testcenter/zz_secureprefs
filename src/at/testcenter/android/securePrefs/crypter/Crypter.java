package at.testcenter.android.securePrefs.crypter;

public interface Crypter {
	public String encrypt(String value) throws Exception;

	public String decrypt(String value) throws Exception;
}
