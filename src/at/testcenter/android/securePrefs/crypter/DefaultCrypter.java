package at.testcenter.android.securePrefs.crypter;

import at.testcenter.android.securePrefs.crypter.impl.Base64Crypter;

public final class DefaultCrypter {
	/**
	 * NOTE: this will not work for api level <8 <br>
	 * if api < 8, the given string will be returned!
	 */
	public final static Base64Crypter base64Crypter = new Base64Crypter();
}
