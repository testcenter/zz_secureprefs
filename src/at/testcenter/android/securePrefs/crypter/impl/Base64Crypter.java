package at.testcenter.android.securePrefs.crypter.impl;

import android.util.Base64;
import at.testcenter.android.securePrefs.crypter.Crypter;

/**
 * @author lucas
 * 
 * 
 *         NOTE: if api level is < 8, the given value will be returned!!
 */
public class Base64Crypter implements Crypter {

	@Override
	public String encrypt(String value) throws Exception {
		if (android.os.Build.VERSION.SDK_INT < 8) {
			return value;
		}

		byte[] encoded = Base64.encode(value.getBytes(), Base64.DEFAULT);
		String encodedString = new String(encoded);
		return encodedString;
	}

	@Override
	public String decrypt(String value) {
		if (android.os.Build.VERSION.SDK_INT < 8) {
			return value;
		}

		byte[] decoded = Base64.decode(value.getBytes(), Base64.DEFAULT);
		String decodedString = new String(decoded);
		return decodedString;
	}
}
