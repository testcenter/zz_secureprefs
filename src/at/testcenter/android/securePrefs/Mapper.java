package at.testcenter.android.securePrefs;

/**
 * @author lucas
 * 
 * @param <T>
 */
public interface Mapper<T> {
	/**
	 * marshall an object to string (best use gson or json)
	 * @param object
	 * @return
	 */
	public String marshall(T object);

	/**
	 * unmarshall an object from string to object (best use gson or json)
	 * @param s
	 * @return
	 */
	public T unmarshall(String s);
}
